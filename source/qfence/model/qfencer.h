#pragma once

#include "qfence/model/ifencer.h"
#include "qfence/model/fence_fsm.h"

namespace qfence
{
   struct Q
   {
      Q() : m_utility() {}
      
      //m_q[myState][opponentState][action]
      float m_utility[State::num_states][State::num_states][State::num_states];  
      
   };

   class QFencer : public IFencer
   {
   public:
      QFencer(
         float learning_rate,
         float discount_factor,
         float exploration_rate);

      //IFencer
      State state() const override;
      void state(State state) override;
      State action(State opponentState, std::default_random_engine& engine) const override;
      void learn(State myState, State myAction, State opponentState, State opponentAction) override;

      //
      Q const& q() const;
      void setExplorationRate(float exploration_rate);

   private:
      float m_learning_rate;
      float m_discount_factor;
      float m_exploration_rate;
      Q m_q;
      FenceFSM m_fsm;
   };
}
#pragma once

#include "qfence/model/ifencer.h"

namespace qfence
{
   //   come_down    <---    jump_up        prepare_attack  --> attack        
   //        |                     ^        ^                     |
   //        |                      \      /                      |
   //        |                       \    /                       |
   //        --------------------->   idle  <----------------------
   //                                |   ^
   //                                 ---
   class FenceFSM
   {
   public:
      FenceFSM();
      void state(State state);
      State state() const;
      std::vector<State> const& actions() const;
   private:
      State m_state;
      std::vector<State> m_actions[State::num_states];
   };
}
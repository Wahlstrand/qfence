#pragma once

#include <random>

namespace qfence
{
   enum State
   {
      idle,
      prepare_attack,
      attack,
      jump_up,
      come_down,
      num_states
   };

   class IFencer
   {
   public:
      ~IFencer() {}

      virtual State state() const = 0;
      virtual void state(State state) = 0;
      virtual State action(State opponentState, std::default_random_engine& engine) const = 0;
      virtual void learn(State myState, State myAction, State opponentState, State opponentAction) = 0;
   };
}
#pragma once

#include <random>

#include "qfence/model/ifencer.h"
#include "qfence/model/game_state_fwd.h"
#include "qfence/controller/util.h"

namespace qfence
{
   struct GameState
   {
   public:
      GameState(IFencer& p1, IFencer& p2);

      IFencer& m_p1;
      IFencer& m_p2;  

      util::Result m_standings;
   };
}
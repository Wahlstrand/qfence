#include "qfence/model/rfencer.h"

#include "qfence/util/util.h"

using namespace qfence;

//IFencer
State RFencer::state() const
{
   return m_fsm.state();
}
void RFencer::state(State state)
{
   m_fsm.state(state);
}
State RFencer::action(State opponentState, std::default_random_engine& engine) const
{
   return pickRandom(m_fsm.actions(), engine);
}
void RFencer::learn(State myState, State myAction, State opponentState, State opponentAction)
{
}

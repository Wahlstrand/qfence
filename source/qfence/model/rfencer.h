#pragma once

#include "qfence/model/ifencer.h"
#include "qfence/model/fence_fsm.h"

namespace qfence
{
   class RFencer : public IFencer
   {
   public:
      //IFencer
      State state() const override;
      void state(State state) override;
      State action(State opponentState, std::default_random_engine& engine) const override;
      void learn(State myState, State myAction, State opponentState, State opponentAction) override;
   private:
      FenceFSM m_fsm;
   };
}
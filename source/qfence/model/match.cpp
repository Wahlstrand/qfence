#include "qfence/model/match.h"

namespace qfence
{
  Match::Match(
    std::string name,
    std::unique_ptr<IFencer>&& upFencer1,
    std::unique_ptr<IFencer>&& upFencer2)
    : m_name(name)
    , m_upFencer1(std::move(upFencer1))
    , m_upFencer2(std::move(upFencer2))
  {
  }
}
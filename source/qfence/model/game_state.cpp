#include "qfence/model/game_state.h"

using namespace qfence;

GameState::GameState(IFencer& p1, IFencer& p2)
   : m_p1(p1)
   , m_p2(p2)
   , m_standings()
{
   m_p1.state(State::idle);
   m_p2.state(State::idle);
}
#pragma once

#include <string>
#include <memory>
#include "qfence/model/ifencer.h"

namespace qfence
{
  struct Match
  {
    Match(
      std::string name,
      std::unique_ptr<IFencer>&& upFencer1,
      std::unique_ptr<IFencer>&& upFencer2);

    std::string m_name;
    std::unique_ptr<IFencer> m_upFencer1;
    std::unique_ptr<IFencer> m_upFencer2;
  };

}
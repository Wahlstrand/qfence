#include "qfence/model/qfencer.h"

#include "qfence/util/util.h"

using namespace qfence;

namespace {
   State bestAction(Q const& q, std::vector<State> const& actions, State myState, State opponentState)
   {
      if (actions.empty())
      {
         throw std::runtime_error("No available actions");
      }

      float bestQ = -std::numeric_limits<float>::max();
      State bestAction = State::idle;
      for (auto action : actions)
      {
         float thisQ = q.m_utility[myState][opponentState][action];
         if (thisQ >= bestQ)
         {
            bestQ = thisQ;
            bestAction = action;
         }
      }
      return bestAction;
   }
   void qLearn(
      Q& q,
      State myState,
      State myAction,
      State opponentState,
      State opponentAction,
      std::vector<State> const& newAvailableActions,
      float learning_rate,
      float discount_factor,
      float reward)
   {
      State nextBestAction = bestAction(q, newAvailableActions, myAction, opponentAction);
      q.m_utility[myState][opponentState][myAction] =
         (1.0f - learning_rate) * q.m_utility[myState][opponentState][myAction] +
         learning_rate * (reward + discount_factor * q.m_utility[myAction][opponentAction][nextBestAction]);
   }
}

QFencer::QFencer(
   float learning_rate,
   float discount_factor,
   float exploration_rate)
   : m_learning_rate(learning_rate)
   , m_discount_factor(discount_factor)
   , m_exploration_rate(exploration_rate)
   , m_q()
{
}

//IFencer
State QFencer::state() const
{
   return m_fsm.state();
}
void QFencer::state(State state)
{
   m_fsm.state(state);
}
State QFencer::action(State opponentState, std::default_random_engine& engine) const
{
   std::uniform_real_distribution<float> dist(0.0f, 1.0f);
   if (dist(engine) >= m_exploration_rate)
   {
      return bestAction(m_q, m_fsm.actions(), m_fsm.state(), opponentState);
   }
   else
   {
      return pickRandom(m_fsm.actions(), engine);
   }
}
void QFencer::learn(State myState, State myAction, State opponentState, State opponentAction)
{
   //Compute reward
   float reward = eval(myAction, opponentAction);

   //Learn from this
   qLearn(
      m_q,
      myState,
      myAction,
      opponentState,
      opponentAction,
      m_fsm.actions(),
      m_learning_rate,
      m_discount_factor,
      reward);
}

//
Q const& QFencer::q() const 
{ 
   return m_q;
}
void QFencer::setExplorationRate(float exploration_rate)
{
   m_exploration_rate = exploration_rate;
}

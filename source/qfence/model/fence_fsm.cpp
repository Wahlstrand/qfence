#include "qfence/model/fence_fsm.h"

using namespace qfence;

FenceFSM::FenceFSM()
   : m_state(State::idle)
{
   m_actions[State::idle].push_back(State::jump_up);
   m_actions[State::idle].push_back(State::prepare_attack);
   m_actions[State::idle].push_back(State::idle);

   m_actions[State::prepare_attack].push_back(State::attack);
   m_actions[State::attack].push_back(State::idle);

   m_actions[State::jump_up].push_back(State::come_down);
   m_actions[State::come_down].push_back(State::idle);
}

void FenceFSM::state(State state)
{
   m_state = state;
}
State FenceFSM::state() const 
{ 
   return m_state; 
}
std::vector<State> const& FenceFSM::actions() const
{ 
   return m_actions[m_state]; 
}
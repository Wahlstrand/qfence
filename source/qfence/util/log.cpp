#include "qfence/util/log.h"

#include <iostream>
#include <sstream>
#include <fstream>
#include <iomanip>

#include "qfence/model/qfencer.h"

namespace qfence::util::log
{
  std::string toString(State state)
  {
    switch (state)
    {
    case idle:
      return "idle";
    case prepare_attack:
      return "prepare_attack";
    case attack:
      return "attack";
    case jump_up:
      return "jump_up";
    case come_down:
      return "come_down";
    }
    return "UNKNOWN";
  }
  void printQ(std::string const& name, Q const& q)
  {
    std::vector<State> my_states = { State::idle };
    std::vector<State> my_actions = { State::idle, State::prepare_attack, State::jump_up };
    std::vector<State> opponent_states = { State::idle, State::prepare_attack, State::attack, State::jump_up, State::come_down };

    std::cout << "***" << name << "***\n";
    for (auto my_state : my_states)
    {
      for (auto opponent_state : opponent_states)
      {
        std::cout << '\t' << toString(my_state) << " vs " << toString(opponent_state) << "\n";

        for (auto action : my_actions)
        {
          std::cout << "\t\tQ[action = " << toString(action) << "] = " << q.m_utility[my_state][opponent_state][action] << '\n';
        }
      }
      std::cout << '\n';
    }
  }


  void writeQs(std::vector<QLog> const& qs)
  {
    std::vector<State> my_states = { State::idle };
    std::vector<State> my_actions = { State::idle, State::prepare_attack, State::jump_up };
    std::vector<State> opponent_states = { State::idle, State::prepare_attack, State::attack, State::jump_up, State::come_down };

    for (auto my_state : my_states)
    {
      for (auto opponent_state : opponent_states)
      {
        std::stringstream ss;
        ss << "q_" << toString(my_state) << "_vs_" << toString(opponent_state) << ".csv";
        std::ofstream ofs(ss.str(), std::ofstream::out);
        ofs << std::setprecision(3);
        
        //Header
        ofs << "action";
        for (auto&& q : qs)
        {
          ofs << ", " << q.m_iterations;
        }
        ofs << '\n';

        for (auto action : my_actions)
        {
          ofs << toString(action);
          for (auto&& q : qs)
          {
            ofs << ", " << q.m_q.m_utility[my_state][opponent_state][action];
          }
          ofs << '\n';
        }

        ofs.close();
      }
    }
  }
}
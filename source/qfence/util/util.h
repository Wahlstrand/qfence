#pragma once

#include <stdexcept>
#include <random>

#include "qfence/model/ifencer.h"

namespace qfence
{
   bool hit(State s1, State s2);
   bool parry(State s1, State s2);
   float eval(State s1, State s2);

   template <typename TypeT>
   TypeT pickRandom(std::vector<TypeT> const& elements, std::default_random_engine& engine)
   {
      if (elements.empty())
      {
         throw std::runtime_error("No available elements!");
      }
      std::uniform_int_distribution<size_t> dist(0, elements.size() - 1);
      return elements[dist(engine)];
   }
}
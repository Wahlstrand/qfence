#include "qfence/util/util.h"

namespace qfence
{
   bool hit(State s1, State s2)
   {
      return
         s1 == State::attack &&
         s2 != State::jump_up &&
         s2 != State::come_down;
   }
   bool parry(State s1, State s2)
   {
      return
         s2 == State::attack && s1 == State::jump_up ||
         s2 == State::attack && s1 == State::come_down;
   }
   float eval(State s1, State s2)
   {
      static float const hit_value = 1.0f;
      static float const parry_value = 1.0f;
      
      //hit
      if (hit(s1, s2) && hit(s2, s1))
      {
        return 0.0f;
      }
      else if (hit(s1, s2))
      {
         return hit_value;
      }
      else if (hit(s2, s1))
      {
         return -hit_value;
      }

      //parry
      if (parry(s1, s2))
      {
         return parry_value;
      }
      else if (parry(s2, s1))
      {
         return -parry_value;
      }

      return 0.0;
   }
}
#pragma once

#include <string>
#include "qfence/model/game_state_fwd.h"
#include "qfence/model/qfencer.h"

namespace qfence::util::log
{
  struct QLog
  {
    size_t m_iterations;
    Q m_q;
  };

  std::string toString(State state);
  void printQ(std::string const& name, Q const& q);
  void writeQs(std::vector<QLog> const& qs);
}
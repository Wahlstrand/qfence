#include "qfence/controller/util.h"

#include "qfence/util/util.h"
#include "qfence/model/game_state.h"

namespace qfence::util
{
   Result::Result()
      : m_p1()
      , m_p2()
   {
   }
   Result::Player::Player()
      : m_hit()
      , m_dodge()
   {
   }

   void train(IFencer& p1, IFencer& p2, size_t runs, std::default_random_engine& engine)
   {
      for (size_t n = 0; n < runs; n++)
      {
         //Select actions
         State s1 = p1.state();
         State a1 = p1.action(p2.state(), engine);
         State s2 = p2.state();
         State a2 = p2.action(p1.state(), engine);

         //Update state
         p1.state(a1);
         p2.state(a2);

         //Learn from this
         p1.learn(s1, a1, s2, a2);
         p2.learn(s2, a2, s1, a1);
      }
   }

   void step(GameState& gameState, std::default_random_engine& engine)
   {
      auto& p1 = gameState.m_p1;
      auto& p2 = gameState.m_p2;

      //Select actions
      State s1 = p1.state();
      State a1 = p1.action(p2.state(), engine);
      State s2 = p2.state();
      State a2 = p2.action(p1.state(), engine);

      //Update state
      p1.state(a1);
      p2.state(a2);

      //Compute score
      if (hit(a1, a2))
      {
         gameState.m_standings.m_p1.m_hit++;
      }
      if (hit(a2, a1))
      {
         gameState.m_standings.m_p2.m_hit++;
      }
      if (parry(a1, a2))
      {
         gameState.m_standings.m_p1.m_dodge++;
      }
      if (parry(a2, a1))
      {
         gameState.m_standings.m_p2.m_dodge++;
      }
   }
}
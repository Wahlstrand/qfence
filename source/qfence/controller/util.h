#pragma once

#include <random>

#include "qfence/model/ifencer.h"
#include "qfence/model/game_state_fwd.h"

namespace qfence::util
{
   struct Result
   {
      struct Player {
         Player();
         size_t m_hit;
         size_t m_dodge;
      };

      Result();
      Player m_p1;
      Player m_p2;
   };
   void train(IFencer& p1, IFencer& p2, size_t runs, std::default_random_engine& engine);
   //Result duel(IFencer& p1, IFencer& p2, size_t runs, std::default_random_engine& engine);
   void step(GameState& gameState, std::default_random_engine& engine);
}
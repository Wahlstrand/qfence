#pragma once

#include <memory>
#include <unordered_map>

#include "qfence/model/game_state.h"

#include <SFML/Graphics/RenderWindow.hpp>
#include <SFML/Graphics/Sprite.hpp>
#include <SFML/Graphics/Texture.hpp>
#include <SFML/Graphics/Font.hpp>
#include <SFML/Window/Keyboard.hpp>

namespace qfence
{
   struct UserInput
   {
      UserInput();
      bool m_quit;
      bool m_increment;
      bool m_decrement;
   };

   class Gui
   {
   public:
      Gui(std::string const& title);
      void update(GameState const& state);
      void title(std::string const& title);
      UserInput const& input() const;
   
   private:
      ::sf::RenderWindow m_window;
      UserInput m_input;
      sf::Texture m_spriteSheet;
      sf::Font m_font;
      std::unordered_map<State, sf::Sprite> m_sprites;
      std::string m_title;

      sf::Sprite makeSprite(sf::IntRect const& rect, sf::Vector2f const& origin);

      void processEvents();
      void keyPress(sf::Keyboard::Key);
      void keyRelease(sf::Keyboard::Key);
      void draw(GameState const& state);

      enum class KeyState
      {
        unknown = 0,
        down,
        up
      };
      std::unordered_map<::sf::Keyboard::Key, KeyState> m_keyState;
   };
}
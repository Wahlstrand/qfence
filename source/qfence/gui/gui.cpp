#include "qfence/gui/gui.h"

#include <sstream>

#include <SFML/Window/Event.hpp>
#include <SFML/Graphics/Text.hpp>

using namespace qfence;

UserInput::UserInput()
  : m_quit(false)
  , m_increment(false)
  , m_decrement(false)
{
}

Gui::Gui(std::string const& title)
  : m_window(
    ::sf::VideoMode(460, 400),
    "QFence",
    ::sf::Style::Default,
    ::sf::ContextSettings(24U, 8U, 8U))
  , m_input()
  , m_spriteSheet()
  , m_font()
  , m_sprites()
  , m_title(title)
{
  if (!m_spriteSheet.loadFromFile("fighter.gif"))
  {
    throw std::runtime_error("Failed to load sprite sheet.");
  }
  if (!m_font.loadFromFile("consolas.ttf"))
  {
    throw std::runtime_error("Failed to load font.");
  }

  m_spriteSheet.setSmooth(true);

  m_sprites[State::idle] = makeSprite(
    sf::IntRect(290, 200, 35, 50),
    sf::Vector2f(17.0f, 44.0f));
  m_sprites[State::prepare_attack] = makeSprite(
    sf::IntRect(442, 335, 45, 41),
    sf::Vector2f(21.0f, 34.0f));
  m_sprites[State::attack] = makeSprite(
    sf::IntRect(424, 400, 60, 29),
    sf::Vector2f(45.0f, 23.0f));
  m_sprites[State::jump_up] = makeSprite(
    sf::IntRect(22, 128, 33, 55),
    sf::Vector2f(17.0f, 44.0f));
  m_sprites[State::come_down] = makeSprite(
    sf::IntRect(58, 128, 45, 56),
    sf::Vector2f(23.0f, 44.0f));
}
void Gui::update(GameState const& game)
{
  processEvents();
  m_window.clear();
  draw(game);
  m_window.display();
}
void Gui::title(std::string const& title)
{
  m_title = title;
}
UserInput const& Gui::input() const
{
  return m_input;
}

sf::Sprite Gui::makeSprite(
  sf::IntRect const& rect,
  sf::Vector2f const& origin)
{
  sf::Sprite sprite;
  sprite.setTexture(m_spriteSheet);
  sprite.setTextureRect(rect);
  sprite.setOrigin(origin);
  return sprite;
}
void Gui::processEvents()
{
  ::sf::Event event;

  m_input.m_quit = false;
  m_input.m_increment = false;
  m_input.m_decrement = false;

  while (m_window.pollEvent(event))
  {
    switch (event.type)
    {
    case ::sf::Event::Closed:
      m_input.m_quit = true;
      break;
    case ::sf::Event::KeyPressed:
      keyPress(event.key.code);
      break;
    case ::sf::Event::KeyReleased:
      keyRelease(event.key.code);
      break;
    default:
      break;
    }
  }
}
void Gui::draw(GameState const& state)
{
  IFencer const& p1 = state.m_p1;
  IFencer const& p2 = state.m_p2;

  static float const titleY = 20.0f;
  static float const labelX = 30.0f;
  static float const hitY = 250.0f;
  static float const dodgeY = 290.0f;
  static float const p1X = 160;
  static float const p2X = 300;
  static float const pY = 220;
  static float const scale = 3.0f;

  //TODO: Clean up copy-pastyness here
  sf::Text titleLabel(m_title, m_font, 32);
  titleLabel.setStyle(sf::Text::Bold);
  titleLabel.setPosition(labelX, titleY);
  m_window.draw(titleLabel);

  sf::Text dodgeLabel("Hit:", m_font, 24);
  dodgeLabel.setStyle(sf::Text::Bold);
  dodgeLabel.setPosition(labelX, hitY);
  m_window.draw(dodgeLabel);

  sf::Text hitLabel("Dodge:", m_font, 24);
  hitLabel.setStyle(sf::Text::Bold);
  hitLabel.setPosition(labelX, dodgeY);
  m_window.draw(hitLabel);

  sf::Text hit1(std::to_string(state.m_standings.m_p1.m_hit), m_font, 24);
  hit1.setPosition(p1X, hitY);
  m_window.draw(hit1);

  sf::Text hit2(std::to_string(state.m_standings.m_p2.m_hit), m_font, 24);
  hit2.setPosition(p2X, hitY);
  m_window.draw(hit2);

  sf::Text dodge1(std::to_string(state.m_standings.m_p1.m_dodge), m_font, 24);
  dodge1.setPosition(p1X, dodgeY);
  m_window.draw(dodge1);

  sf::Text dodge2(std::to_string(state.m_standings.m_p2.m_dodge), m_font, 24);
  dodge2.setPosition(p2X, dodgeY);
  m_window.draw(dodge2);

  sf::Sprite sprite1 = m_sprites[p1.state()];
  sprite1.setScale(-scale, scale);
  sprite1.setPosition(p1X, pY);
  m_window.draw(sprite1);

  sf::Sprite sprite2 = m_sprites[p2.state()];
  sprite2.setScale(scale, scale);
  sprite2.setPosition(p2X, pY);
  m_window.draw(sprite2);
}


void Gui::keyPress(sf::Keyboard::Key key)
{
  if (m_keyState[key] == KeyState::down)
  {
    //Already down
    return;
  }
  
  m_keyState[key] = KeyState::down;

  if (key == ::sf::Keyboard::Q)
  {
    m_input.m_quit = true;
  }
  else if (key == ::sf::Keyboard::Add)
  {
    m_input.m_increment = true;
  }
  else if (key == ::sf::Keyboard::Subtract)
  {
    m_input.m_decrement = true;
  }
}
void Gui::keyRelease(sf::Keyboard::Key key)
{
  m_keyState[key] = KeyState::up;
}

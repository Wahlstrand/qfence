#include <iostream>
#include <iomanip>
#include <memory>
#include <chrono>
#include <thread>
#include <sstream>

#include "qfence/model/qfencer.h"
#include "qfence/model/rfencer.h"
#include "qfence/model/match.h"
#include "qfence/model/game_state.h"
#include "qfence/gui/gui.h"
#include "qfence/controller/util.h"
#include "qfence/util/log.h"

using namespace qfence;

using namespace std::chrono_literals;

namespace
{
  std::vector<Match> makeMatches()
  {
    static float const learning_rate = 0.05f;
    static float const discount_factor = 0.8f;
    static float const exploration_rate = 0.1f;
    std::default_random_engine engine;

    std::vector<Match> matches;
    std::vector<util::log::QLog> qs;
    for (size_t training_rounds = 1; training_rounds <= pow(2,25); training_rounds *= 2)
    {
      auto upFencer1 = std::make_unique<QFencer>(learning_rate, discount_factor, exploration_rate);
      auto upFencer2 = std::make_unique<RFencer>();

      util::train(*upFencer1, *upFencer2, training_rounds, engine);
      upFencer1->setExplorationRate(0.0f);

      qs.push_back({ training_rounds, upFencer1->q() });

      std::stringstream ss;
      ss << "Iterations: " << training_rounds;

      qfence::util::log::printQ(ss.str(), upFencer1->q());

      matches.emplace_back(
        ss.str(),
        std::move(upFencer1),
        std::move(upFencer2));
    }
    
    util::log::writeQs(qs);

    return matches;
  }
}

int main(int, char**)
{
  //Setup game
  std::default_random_engine engine(3);
  auto matches = makeMatches();
  auto currentMatch = matches.begin();

  std::unique_ptr<GameState> upGame = std::make_unique<GameState>(*currentMatch->m_upFencer1, *currentMatch->m_upFencer2);
  Gui gui(currentMatch->m_name);
  while (true)
  {
    util::step(*upGame, engine);
    gui.update(*upGame);
    auto const& input = gui.input();
    if (input.m_quit)
    {
      return 0;
    }
    else if (input.m_increment)
    {
      if (currentMatch != std::prev(matches.end()))
      {
        currentMatch++;
      }
     
      upGame.reset(new GameState(*currentMatch->m_upFencer1, *currentMatch->m_upFencer2));
      gui.title(currentMatch->m_name);
    }
    else if (input.m_decrement)
    {
      if (currentMatch != matches.begin())
      {
        currentMatch--;
      }

      upGame.reset(new GameState(*currentMatch->m_upFencer1, *currentMatch->m_upFencer2));
      gui.title(currentMatch->m_name);
    }

    std::this_thread::sleep_for(100ms);
  }

  return 0;
}